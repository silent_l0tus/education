def getMatrix(n, m, value):
    if n <= 0:
        return []
    elif m <= 0:
        return []
    elif value <= 0:
        return []
    else:
        matrix = []
        for i in range(0, n):
            matrix.append([])
            for j in range(0, m):
                matrix[i].append(value)
    return matrix


result1 = getMatrix(2, 2, 10)
result2 = getMatrix(3, 5, 42)
result3 = getMatrix(4, 2, 13)
print(result1)
print(result2)
print(result3)
