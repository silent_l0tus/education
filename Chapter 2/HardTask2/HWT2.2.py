import random


def key():
    k = random.randrange(3, 20)
    return k


k = key()
print(k, "- ", end=' ')

for i in range(1, k):
    for j in range(i + 1, k):
        if (i + j) % k == 0:
            print(i, j, end=' ')
