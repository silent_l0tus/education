calls = 0


def countCalls():
    global calls
    calls += 1
    return


def stringInfo(string):
    global calls
    calls += 1
    c = (len(string), string.lower(), string.upper())
    return c


def isContains(string, listOfSerch):
    global calls
    calls += 1
    for i in listOfSerch:
        if string.casefold() == i.casefold():
            return True
    return False


print(stringInfo('Capybara'))
print(stringInfo('Armageddon'))
print(isContains('Urban', ['ban', 'BaNaN', 'urBAN']))  # Urban ~ urBAN
print(isContains('cycle', ['recycling', 'cyclic']))  # No matches
print(calls)
