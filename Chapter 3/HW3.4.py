def singleRootWords(root_word, *other_words):
    sameWords = []
    root_word=root_word.casefold()

    for i in other_words:
        if root_word in i.casefold():
            sameWords.append(i)
        elif i.casefold() in root_word:
            sameWords.append(i)
    return sameWords


result1 = singleRootWords('rich', 'richiest', 'orichalcum', 'cheers', 'richies')
result2 = singleRootWords('Disablement', 'Able', 'Mable', 'Disable', 'Bagel')
print(result1)
print(result2)
