def get_multiplied_digits(number):
    strNumber = str(number)
    first = int(strNumber[0])
    if len(strNumber) > 1:
        a = first * get_multiplied_digits(int(strNumber[1:]))
        return a
    else:
        return first

result = get_multiplied_digits(40203)
print(result)
